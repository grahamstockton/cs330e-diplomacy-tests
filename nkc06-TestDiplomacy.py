#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_printer

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC Amarillo Move Austin\nD Paris Support B\nE ElPaso Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC [dead]\nD Paris\nE ElPaso\n")

    def test_solve_2(self):
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC Amarillo Move Nagadoches\nD Paris Support B\nE ElPaso Support D\nF SanAntonio Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Nagadoches\nD Paris\nE ElPaso\nF [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")
# ----
# main
# ----


if __name__ == "__main__":
    main()
